---
layout: handbook-page-toc
title: "GitLab Channel Program Guide"
description: "GitLab Channel partner program guide."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

**We invite organizations interested in becoming a GitLab Channel Partner to [APPLY NOW](https://partners.gitlab.com/English/register_email.aspx).**

## Introduction

The GitLab Channel Partner Program offers market-leading training, tools, and support to help accelerate sales opportunities and grow your business with GitLab. The following information is intended to help our partners understand our program.

## Program Requirements, Benefits, and Guidelines

The GitLab Partner Program consists of two tracks that support the different ways our partners go to market, Open and Select:

**1. Open –** The Open track is for all partners (resellers, integrators, sales, and services) in the DevOps space, IT as a service (ITaaS) and other adjacent spaces that are committed to investing in their DevOps practice buildout. The Open track is also for partners seeking to develop customers or that just want to learn about GitLab and participate in the GitLab partner community. 

Partners may or may not be transacting partners, and qualifying partners can earn product discounts, referral fees and partner services incentives. We provide:
* Broad Sales and Services Enablement
* Discounts and Referral Fees
* GitLab Badges   
* Field Support

Open partners must meet the minimum requirements outlined in the section Program Requirements and Benefits below to earn program benefits, including:
* Completion of GitLab Sales Core (Verification) in the region specified in your GitLab Partner Contract.
* Technical Sales certifications in the region specified in your GitLab Partner Contract. Both the GitLab Sales Architect Core and Professional Services Engineering courses meet this requirement.

**2. Select –** In this track, partners make a greater investment in expertise, develop services practices around GitLab, and are expected to drive greater product recurring revenues. In return, Select partners have a dedicated Channel Sales Manager and greater investment. Participation in the Select track is by invitation only. We provide:
* Focused Strategic Relationships (Channel Account Manager, Solution Architect, Executives)
* Discounts and Referral Fees, including limited-time Select-partner-only incentive programs
* Targeted Sales and Services Enablement
* Badges and Certifications
* Access to Proposal-Based MDF
* Lead-Sharing Opportunities
* Ultimate-Level Technical Support
* Business Planning and Quarterly Business Reviews (QBRs)
* Priority Help Desk Support
* Priority Position in the GitLab Partner Locator

Since participation in the Select track is by invitation only, partners are not automatically upgraded. To be considered, they must meet the requirements outlined in Program Requirements and Benefits below, including:
* Verifications (completion of GitLab Sales Core) and certification (Sales Core plus Advanced Use Case Training) in the region specified in your GitLab Partner Contract.
* Technical Sales certifications in the region specified in your GitLab Partner Contract. Both the GitLab Sales Architect Core and Professional Services Engineering courses meet this requirement.
* Professional Services certification.
* Executive-sponsored joint business plan.
* Minimum program revenue, defined as Net Annual Recurring Revenue (NetARR). This includes all the incremental licenses sold to new and existing customers.
* Dedicated DevOps sales and/or services practice.
* Minimum of one GitLab demand-generation activity per quarter.

## Program Requirements and Benefits

|   |**GitLab Open** | **GitLab Select** <br> **(Invitation only)** |
|------------------|:---------------------------:|:---:|
| **Overall Program Requirements** |  |  |
| GitLab Partner Agreement | X | X |
| # of Sales Verified (Sales Core) resources | 2 | 4 |
| # of GitLab Sales Professional Certificatied resources |   | 1 |
| # of Solution Architect Verified resources | 1 | 2 |
| # of Professional Services Certified resources |   | 1 |
| Exec Sponsored Joint Business Plan |  | X |
| Program Rev Targets Min - BusPlan Committed |  | $300K |
|  |  |  |
| **Sales Benefits** |  |  |
| Deal Registration Discounts | X | X |
| Referral Fee | X | X |
| Renewals Discounts | X | X |
|  |  |  |
| **Support Benefits** |  |  |
| Partner Helpdesk | X | X |
| Level 2 Support Hotline |  | X |
| Dedicated Channel Manager |  | X |
| Not for Resale Licenses | X | X |
|  |  |  |
| **Marketing** |  |  |
| Partner Portal | X | X |
| Proposal Based MDF | Through Distribution | X |
| Sales & Pre-Sales Technical Enablement | X | X |
| Program Logo | X | X |
| Program & Certification Badges | X | X |
| GitLab.com Listing | X | X (featured) |
| Demand Generation Resources | X | X |
| Leads |  | X |
|  |  |  |
| **Services** |  |  |
| GitLab Professional Services Discounts | X | X |
| Professional Services Certification | X | X |
| Training Services Certification | X | X |
|  |  |  |
| **Managed Services** |  |  |
| Managed Services certification | X | X |
| Managed Services Discount | X | X |

## More Channel Partner Program Information
If you are looking for additional information on the GitLab Partner program see the following handbook pages.
* [Channel Partner Program Overview](https://about.gitlab.com/handbook/resellers/) 
* [Channel Program Guide](https://about.gitlab.com/handbook/resellers/Channel-Program-Guide/)
* [Channel Services Program](/handbook/resellers/services/)
* [Channel Services Catalog](/handbook/resellers/services/services-catalog/)
* [Channel Partner Training and Certifications](/handbook/resellers/training/)
* [Channel Partners: Working with GitLab](/handbook/resellers/channel-working-with-GitLab/)
* [Channel Tools and Resources](/handbook/resellers/channel-tools-resources/)
* [Alliance Program](https://about.gitlab.com/partners/technology-partners/)
 
## Contact Us
All authorized GitLab resellers are invited to the GitLab #resellers Slack channel. This Slack channel allows you to reach out to our sales and marketing team in a timely manner, as well as other resellers. Additionally, you can reach the GitLab Channel Team at partnersupport@gitlab.com and the GitLab marketing team at partner-marketing@gitlab.com.
