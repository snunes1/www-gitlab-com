---
layout: handbook-page-toc
title: "Executing decisions — TeamOps"
description: Executing decisions — TeamOps
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

![GitLab TeamOps teamwork illustration](/handbook/teamops/images/teamops-illustration_teamwork_purple.png)
{: .shadow.medium.center}

This page is about one of the four Guiding Principles of TeamOps. Get immersed in a [complete overview of TeamOps](/teamops/), or jump straight into the free [TeamOps Practitioner Certification](https://levelup.gitlab.com/learn/course/teamops). 


# Teams exist to deliver results

This is about achieving objectives. Being more informed, making faster decisions, and increasing decision making velocity is only useful if you execute and deliver results. TeamOps empowers ***everyone*** to contribute to meaningful business outcomes. It also recognizes that execution ***is not a one-time event***; rather, it is the establishing of a new baseline on which future iterations are applied.

Action tenets and real-world examples are below.

## No-matrix organization

You should only have **one manager**. Conventional management philosophies may focus on minimizing the shortcomings of matrix organizations (or, "dotted line" reporting structures) but refuse to eliminate them. TeamOps asserts that a [no-matrix organization](/handbook/leadership/no-matrix-organization/) is not only feasible, but *essential* to executing decisions. By ensuring that each individual reports to exactly one other individual, contributions have a clear path to receiving feedback. 

**Here's an example**: Every GitLab team member has exactly one manager

In an organization powered by TeamOps, a team member specializing in sales accounting (in this example, the team member's title is `FP&A Analyst, Sales Finance`) reports to **exactly one person** with domain knowledge (with the title `Sr. Director, Sales Finance`). This *one* manager understands their report's day-to-day tasks, has served in that role prior, is positioned to share a single set of goals, and is suited to coach the team member through a mutually understood career path. The team member does not "dual report" to Finance and Sales. 

Rather than relying on reporting structure to ensure collaboration, [collaboration is installed at a foundational, organization-wide level](/handbook/values/#collaboration). Each function is expected to honor the associated operating principles. Whenever there is a need to work on a specific, high-level, cross functional business problem, assemble a [working group](/company/team/structure/working-groups/) only for the duration of time required to execute.

## Measure results, not hours

In organizations powered by TeamOps, all team members focus on [executing business results](/handbook/values/#measure-results-not-hours), rather than executing on [presenteeism](https://language.work/research/killing-time-at-work/). 

**Here's an example**: [Measuring impact of GitLab's 10 year campaign](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5507)

A cross-functional effort was required to produce the `10 Years of GitLab` integrated marketing campaign and [associated website](/ten/). A GitLab issue was established to explicitly define [elements to be tracked and measured](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5507) in order to provide an impact report. By focusing on results over hours spent (or if a given team member was online at a certain time, or in a certain office), everyone involved in the project can focus energy on execution. 

## Give agency

Masterful execution [requires agency to be given](/handbook/values/#give-agency) by default. Micromanaging stifles execution, but agency *empowers* an individual to focus their time and attention on what they deem best. 

**Here's an example**: [Normalizing that it's OK to look away in video calls](/company/culture/all-remote/meetings/#9-its-ok-to-look-away)

Giving agency begins in the most typical of places. Video calls are a natural part of day-to-day work for many knowledge workers, yet cultural expectations about presenteeism and attentiveness may restrict agency. GitLab explicitly documents that [it's OK to look away](/company/culture/all-remote/meetings/#9-its-ok-to-look-away) during meetings and that no one should be embarrassed to occasionally ask for something to be repeated. By creating a culture where people are free to manage their own time and attention, they're able to direct energy on a minute-by-minute basis to execute. No one's path to execution looks the same. It may involve breaks to connect with friends, taking a walk outside, or watching a recording of a meeting during a more suitable time. 

## Bias for action

Execution is accelerated when a team maintains a [bias for action](/handbook/values/#bias-for-action) as opposed to alternatives like alignment and consensus. When facing decisions that may involve imperfect information or failures, having a bias for action ensures a more rapid pace of execution. This does require a tolerance for mistakes and an appreciation for [two-way door decisions](/handbook/values/#make-two-way-door-decisions), an operating principle linked to [Fast Decisions](/handbook/teamops/fast-decisions/#two-way-door-decisions). 

**Here's an example**: [Coursera Remote Work course development](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1128)

In September 2020, much of the knowledge-working world began to seek training on how to manage remote teams. As the COVID-19 pandemic forced many teams to work primarily from home, GitLab was well-positioned to provide proven practices for others to learn from and contribute back to. [Jessica R.](https://gitlab.com/jessicareeder)'s bias for action led to a question: "*What if we partner with a leader in online learning to teach the world a skill that it needs right now?*" This [GitLab epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1128) is a case study in execution, sparked by a bias for action. Every iteration is documented. The output is [How to Manage a Remote Team](https://www.coursera.org/learn/remote-team-management), a free-to-take course on Coursera with 40,000 learners (and growing!). 

## Boring solutions

In conventional organizations, the fanciest solution may receive the most support. In organizations powered by TeamOps, the most boring solutions are celebrated. There's an art to not overthinking a problem, and lowering one's ego in order to use established, boring solutions already in the market. 

**Here's an example**: [Extend GitLab.com status page with key metrics](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/698)

A GitLab team member sought to [safely publicize key metrics for the benefit of the GitLab.com user base](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/698). Rather than defaulting to a proposal where new technology was to be built, [Joshua L.](https://gitlab.com/joshlambert) proposed a boring solution: use `status.io` for the status page. "This is the most boring solution," Joshua states, "and it presents the metrics on the page we already direct users to for the status."


## Transparent measurements

Conventional management philosophies glorify metrics, which is a nonspecific term that often lacks connection to goals, mission, and strategy. TeamOps prefers [Key Performance Indicators (KPIs)](/company/kpis/), which may be linked to [Objectives and Key Results](/company/okrs/) (OKRs) and offer greater context on their relevance to a function or the entire company. 

OKRs are frequently the delta in KPIs. If you want to improve a KPI, you have an initiative (OKR), and that's what you work on that quarter. If you're not *creating* OKRs to *improve* KPIs, then you're either missing KPIs or you have the wrong OKRs. 

Crucially, KPIs for each function are [transparently shared](/handbook/values/#findability) across the organization. This enables everyone to contribute by creating visibility between departments.

**Here's an example**: [Chief Executive Officer OKR and KPIs](/company/okrs/fy23-q3/)

In Q3-FY23, a CEO OKR is [Improve user and wider-community engagement](/company/okrs/fy23-q3/). This is the *initiative* to improve a series of KPIs, a subset of which are documented below: 
1. Evolve the resident contributor strategy by conducting 5 customer conversations with current “resident contributors” in seat
1. Certify 1,000 team members and 10,000 wider-community members in TeamOps 
1. Enhance Corporate Processes and Successful Corporate Development Integration & Prospecting 

These are documented in a tool (GitLab uses Ally) that is accessible to the entire organization. Critically, any team member can see any other functions OKRs and KPIs for the quarter, reinforcing the [value of transparency](/handbook/values/#transparency).

## Iteration enables execution

Executing on a decision is not binary. It is also not a one-time event. TeamOps reframes execution as an ongoing series of [iterations](/handbook/values/#iteration), with each one worthy of celebration. This encourages [smaller steps](/handbook/values/#move-fast-by-shipping-the-minimal-viable-change), which are more amenable to feedback and course correction. By breaking decisions down into manageable components, execution is more feasible. 

**Here's an example**: [Adding an MVC GitLab Citation Index to GitLab for Education homepage](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69665/)

In a conventional organization, a revised homepage could be seen as binary. It's either complete, or not. [This merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69665/) details a minimum viable change (MVC) to add one element to the [GitLab for Education](/solutions/education/) homepage. In the comment thread, GitLab team members agree that this iteration moves the page one step closer to an ideal state. By embracing and celebrating each iteration *as* execution, it enables team members to accelerate execution on other projects rather than being stuck on a prior project.  

## Single source of truth (SSoT)

Having multiple sources of truth slows a team's ability to deliver results. Executing on decisions happens much faster when there is a [single source of truth](/handbook/values/#single-source-of-truth). TeamOps empowers everyone to contribute status updates and improvements within an agreed-upon documentation system (the [handbook](/handbook/handbook-usage/) at GitLab; mediums such as [Almanac](https://almanac.io/), [Guru](https://www.getguru.com/), and [Notion](https://www.notion.so/) for other organizations). Honoring the single source of truth begins at the earliest stage of execution.

**Here's an example**: [Establishing a SSoT for dates and time zones at GitLab](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107060)

Several GitLab team members recognized that there were competing sources of truth for communicating dates and time zones in the company handbook. The discussion began as a Slack thread and quickly [moved to a merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107060) where execution could occur. Crisp communication is critical to executing decisions, especially in a distributed workplace where synchronous engagements are more rare and work spans numerous time zones. 

## Prioritize due dates over scope

TeamOps requires due dates. This is not a means to create unnecessary rigidity, but to force mechanisms that enable teams to execute on decisions.

A TeamOps organization will always [set a due date](/handbook/values/#set-a-due-date), and if necessary, will cut scope to meet the due date rather than postpone the date. This enables everyone to contribute to future progress on adding features in a future iteration (or future *execution*), while limiting the loss of momentum.

**Here's an example**: [Maintaining a monthly release cadence for 10+ years](/releases/)

As of April 30, 2022, GitLab has shipped a monthly product release for [127 consecutive quarters](https://ir.gitlab.com/news-releases/news-release-details/gitlab-reports-first-quarter-fiscal-year-2023-financial-results). That's over 10 years! A decade of [consistent execution](/blog/2018/11/21/why-gitlab-uses-a-monthly-release-cycle/) is made possible by [cutting scope](/handbook/values/#set-a-due-date) instead of pushing ship dates. 

## Disagree, commit, and disagree

"Disagree and commit" has become a somewhat commonly understood phrase in professional settings. In an organization powered by TeamOps, a second "disagree" is added to form "[Disagree, commit, and disagree](/handbook/values/#disagree-commit-and-disagree)." This mentality empowers everyone to contribute to change without slowing down the pace of execution. In the more conventional "disagree and commit" framework, execution is encouraged but contributions to future iterations are limited. By explicitly stating that team members are expected to execute (commit) while a decision stands, but are welcome to disagree, it invites everyone to constructively surface dissent and potential proposals for change. 

**Here's an example**: [Require seniors to become maintainers](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/106942)

A GitLab merge request details a policy change to "[increase maintainers by requiring senior engineers to become a maintainer in at least one project/area](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/106942)." 57 team members participated in discussion within the merge request itself, and the DRI for the change confirms in the description that "in a recent survey, 11% of respondents did not want to become a maintainer, and 35% disagreed that it should be a senior+ responsibility." Despite the disagreement, the iteration was merged and thus, a decision was made. Thanks to the operating principle "Disagree, commit, and disagree," anyone is welcome to disagree with the change and influence the next iteration through constructive conversation with the DRI. 

## Key Review meetings

[Key Review meetings](/handbook/key-review/) present a functional group's OKRs, KPIs, how the team is trending toward achieving goals, items that are top of mind, etc. In conventional organizations, this is apt to be a more informal conversation between a department head and their manager. By broadening the audience of attendees for a Key Review meeting to include the Chief Executive Officer (CEO), Chief Financial Officer (CFO), the function head, and (optionally) all other executives and their direct reports, we widen the pool of people who can contribute feedback, insights, and advice. It forces the presenting department to be more mindful of execution, consider areas where they are falling short, and gather input for potential iterations toward progress.  

**Here's an example**: [GitLab User Experience (UX) department Key Review meeting](https://youtu.be/54LAX8UFU9s)

GitLab User Experience (UX) department [livestreamed a Key Review meeting on GitLab Unfiltered](https://youtu.be/54LAX8UFU9s). A distinct element of these meetings is that [no presentations are allowed](/handbook/communication/#no-presenting-in-meetings). For context, each attendee was able to view a presentation prepared ahead of time, with a shared Google Doc agenda used to maintain an orderly and inclusive flow of questions and conversation. You'll notice that executives and their direct reports provide questions and suggestions throughout. There's a distinct conversation on usability beginning at the 13:51 mark where leads from various functions [contribute to improved execution on a 25-second lag recognized in the product](https://youtu.be/54LAX8UFU9s?t=831). 

## Group Conversations

Group Conversations are recurring 25 minute meetings providing regular updates across all GitLab teams on a rotating schedule. It's the same concept and content as the Key Review meeting mentioned above, with one major difference: *all* team members are invited! These meetings are designed to give people context on what other teams outside of their own are focused on (and how they're executing). Execution isn't solely about executing your goals, but also understanding what others are executing. 

**Here's an example**: [Learning and Development Group Conversation](https://youtu.be/OX15Rknk7tM)

GitLab's Learning and Development team hosted a [livestreamed Group Conversation](https://youtu.be/OX15Rknk7tM) in June 2021. [No presentations are allowed](/handbook/communication/#no-presenting-in-meetings) in Group Conversations. Attendees look at the prepared presentation deck in advance and document questions in a shared Google Doc. At the 6:35 mark, an attendee (who is sharing their screen) [notices that a button does not link to the appropriate place](https://youtu.be/OX15Rknk7tM?t=395). This enables the L&D team to create an action item, plan an iteration, and continue to execute on their OKRs/KPIs. 

## Stable counterparts

In a [stable counterparts model](/blog/2018/10/16/an-ode-to-stable-counterparts/) for enabling cross-functional execution, every functional team (e.g. Support) works with the same team members from a different functional team (e.g. Development). As a member of one function, you always know who your partner in another function will be. Stable counterparts are an intentionally chosen structure designed to execute on decisions. They foster collaboration *across functions* by giving people stable counterparts for other functions they need to work with to execute decisions. This enables more social trust and familiarity, which [speeds up decision making](/handbook/teamops/fast-decisions/), facilitates [stronger communication flows](/handbook/teamops/informed-decisions/), and reduces the risk of conflicts. Stable counterparts enhance cross-functional execution without the downsides of a [matrix organization](/handbook/leadership/#no-matrix-organization).

**Here's an example**: [Technical Support stable counterparts](/handbook/support/support-stable-counterparts.html)

Support team members are [assigned a permanent contact](/handbook/support/support-stable-counterparts.html) for a GitLab team member within another function in the company. The ability to build long-term relationships is the foundational benefit of having stable counterparts. Repeated interactions help us understand personal workflows and communication styles, so we know how to most effectively execute decisions with our counterparts.


---

Return to the [TeamOps homepage](/handbook/teamops/). 
